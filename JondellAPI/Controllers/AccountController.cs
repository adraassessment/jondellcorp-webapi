﻿using JCDtos;
using JCServices.Contract;
using JCUtils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace JondellAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountServices accountServices;

        public AccountController(IAccountServices accountServices)
        {
            this.accountServices = accountServices;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index)
            })
            .ToArray();
        }

        [HttpPost("upload")]
        public IActionResult ImportAccountBalance([FromForm(Name = "File")] IFormFile File)
        {
            var file = HttpContext.Request.Form.Files[0];
            var accountBalances = Helper.GetAccountBalances(file);            
            var accountTurnOverDate = Helper.GetLastDateOfMonth(Path.GetFileNameWithoutExtension(File.FileName));

            var monthAccBalance = new MonthAccBalance();
            monthAccBalance.Balace = accountBalances;

            accountServices.UpdateBalance(monthAccBalance, accountTurnOverDate);

            return new JsonResult(new { state = "sucess" });
        }

        [HttpPost("AccountReport")]
        public IActionResult AccountReport([FromBody] ReportSelection reportSelection)
        {
            return new JsonResult(accountServices.AccountReportData(reportSelection));
        }

        [HttpPost("AccountBalanceDetail")]
        public IActionResult AccountBalanceDetail([FromBody] ReportSelection reportSelection)
        {
            var result = accountServices.AccountBalanceDatails(reportSelection.SelectedDate);

            return new JsonResult(result);
        }


    }
}
