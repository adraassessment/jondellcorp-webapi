﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JCDtos
{
    public class AccountBalanceDet
    {
        public string AccountName { get; set; }
        public decimal? AccountBalance { get; set; }

    }
}
