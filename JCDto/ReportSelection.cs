﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JCDtos
{
    public class ReportSelection
    {
        public DateTime SelectedDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

    }
}
