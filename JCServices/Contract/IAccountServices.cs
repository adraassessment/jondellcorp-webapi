﻿using JCDtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace JCServices.Contract
{
    public interface IAccountServices
    {
        void UpdateBalance(MonthAccBalance monthAccBalance,DateTime? selectedDate);
        List<ReportData> AccountReportData(ReportSelection reportSelection);
        List<AccountBalanceDet> AccountBalanceDatails(DateTime selectedDate);
    }
}
