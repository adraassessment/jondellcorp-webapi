﻿using JCDtos;
using JCRepositories.Contract;
using JCServices.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace JCServices.Implimentation
{
    public class AccountBusiness : IAccountServices
    {
        private readonly IAccountRepository accountRepository;

        public AccountBusiness(IAccountRepository accountRepository )
        {
            this.accountRepository = accountRepository;
        }

        public List<AccountBalanceDet> AccountBalanceDatails(DateTime selectedDate)
        {
            return accountRepository.AccountBalanceDatails(selectedDate);
        }

        public List<ReportData> AccountReportData(ReportSelection reportSelection)
        {
            return accountRepository.AccountReportData(reportSelection);
        }

        public void UpdateBalance(MonthAccBalance monthAccBalance, DateTime? selectedDate)
        {
            this.accountRepository.UploadBalance(monthAccBalance,selectedDate);
        }
    }
}
