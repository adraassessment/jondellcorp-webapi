﻿using JCDtos;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;

namespace JCUtils
{
    public static class Helper
    {

        public static List<AccountBalanceDet> GetAccountBalances(IFormFile file)
        {
            var accountBalances = new List<AccountBalanceDet>();
            string sep = "\t";
            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                while (reader.Peek() >= 0)
                {
                    var line = reader.ReadLine();
                    var splitResult= line.Split(sep.ToCharArray());
                    decimal? balance = decimal.TryParse(splitResult[1].Replace(",", ""), out var accBal) ? accBal : (decimal?)null;
                    accountBalances.Add(new AccountBalanceDet() { AccountName= splitResult[0],AccountBalance= balance });
                }
                   

            }


            return accountBalances;
        }

        public static DateTime GetLastDateOfMonth(string fileName)
        {
            var fileSplitter = fileName.Split('-');
            var year = int.TryParse(fileSplitter[0], out int y) ? y : 0;
            var month = int.TryParse(fileSplitter[1], out int m) ? m : 0;

            var firstDayOfMonth = new DateTime(year, month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            return lastDayOfMonth;
        }
    }
}
