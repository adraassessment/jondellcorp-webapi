﻿using JCDtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace JCRepositories.Contract
{
    public interface IAccountRepository
    {
        void UploadBalance(MonthAccBalance monthAccBalance, DateTime? selectedDate);
        List<ReportData> AccountReportData(ReportSelection reportSelection);
        List<AccountBalanceDet> AccountBalanceDatails(DateTime selectedDate);
    }
}
