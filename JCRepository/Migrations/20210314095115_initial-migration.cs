﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JCRepositories.Migrations
{
    public partial class initialmigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccTypes",
                columns: table => new
                {
                    AccTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AccName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Discription = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccTypes", x => x.AccTypeId);
                });

            migrationBuilder.CreateTable(
                name: "AccBalances",
                columns: table => new
                {
                    AccBalanceId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BalanceDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    AccTypeId = table.Column<int>(type: "int", nullable: true),
                    Balance = table.Column<decimal>(type: "decimal(18,4)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccBalances", x => x.AccBalanceId);
                    table.ForeignKey(
                        name: "FK_AccBalances_AccTypes_AccTypeId",
                        column: x => x.AccTypeId,
                        principalTable: "AccTypes",
                        principalColumn: "AccTypeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccBalances_AccTypeId",
                table: "AccBalances",
                column: "AccTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccBalances");

            migrationBuilder.DropTable(
                name: "AccTypes");
        }
    }
}
