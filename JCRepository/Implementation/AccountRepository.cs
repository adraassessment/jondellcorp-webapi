﻿using JCDtos;
using JCRepositories.Contract;
using JCRepositories.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JCRepositories.Implementation
{
    public class AccountRepository : IAccountRepository
    {
        private readonly ApplicationDbContext applicationDbContext;
        public AccountRepository(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }

        public void UploadBalance(MonthAccBalance monthAccBalance, DateTime? selectedDate)
        {
            var accountTypes = applicationDbContext.AccTypes.ToList();

            using (var transaction = applicationDbContext.Database.BeginTransaction())
            {
                try
                {
                    foreach (var item in monthAccBalance.Balace)
                    {
                        var accBalance = new AccBalance();
                        int? accTypeId = null;
                        var accType = accountTypes.FirstOrDefault(f => f.AccName == item.AccountName);
                        if (accType == null)
                        {
                            var createdAccType = new AccType() { AccName = item.AccountName };
                            // create an account
                            var accTypeCreated = this.applicationDbContext.Add(createdAccType);
                            applicationDbContext.SaveChanges();
                            accTypeId = createdAccType.AccTypeId;
                        }
                        else
                        {
                            accTypeId = accType.AccTypeId;

                        }
                        accBalance.AccTypeId = accTypeId;
                        accBalance.Balance = item.AccountBalance;
                        accBalance.BalanceDate = selectedDate;

                        applicationDbContext.Add(accBalance);
                    }

                    applicationDbContext.SaveChanges();
                    transaction.Commit();

                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            
            }
        }


        public List<ReportData> AccountReportData(ReportSelection reportSelection)
        {
            var rptData = from balance in applicationDbContext.AccBalances
                            where balance.BalanceDate > reportSelection.StartDate && balance.BalanceDate < reportSelection.EndDate
                          group balance.Balance by balance.BalanceDate.Value.Year.ToString() + " - " + balance.BalanceDate.Value.Month.ToString() into g
                          select new ReportData { name=g.Key, value=g.Sum(a=>a.Value) };

            return rptData?.ToList();
        }

        public List<AccountBalanceDet> AccountBalanceDatails(DateTime selectedDate)
        {
            var rptData = from balance in applicationDbContext.AccBalances
                          join accType in applicationDbContext.AccTypes on balance.AccTypeId equals accType.AccTypeId
                          where balance.BalanceDate.Value.Month == selectedDate.Month && balance.BalanceDate.Value.Year == selectedDate.Year
                          select new AccountBalanceDet { AccountName = accType.AccName, AccountBalance = balance.Balance };

            return rptData?.ToList();
        }





    }
}
