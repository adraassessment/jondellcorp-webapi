﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace JCRepositories.Models
{
    public partial class AccBalance
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int AccBalanceId { get; set; }
        public DateTime? BalanceDate { get; set; }
        public int? AccTypeId { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal? Balance { get; set; }

        public virtual AccType AccType { get; set; }
    }
}
