﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace JCRepositories.Models
{
    public partial class AccType
    {
        public AccType()
        {
            AccBalances = new HashSet<AccBalance>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AccTypeId { get; set; }
        public string AccName { get; set; }
        public string Discription { get; set; }


        public virtual ICollection<AccBalance> AccBalances { get; set; }
    }
}
