﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace JCRepositories.Models
{
    public partial class JondelCorpContext : DbContext
    {
        public JondelCorpContext()
        {
        }

        public JondelCorpContext(DbContextOptions<JondelCorpContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccBalance> AccBalances { get; set; }
        public virtual DbSet<AccType> AccTypes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=.\\SQLEXPRESS;Database=JondelCorp;User Id=sa;Password=abc@123");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<AccBalance>(entity =>
            {
                entity.ToTable("AccBalance");

                entity.Property(e => e.AccBalanceId)
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.Property(e => e.Balance).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.BalanceDate).HasColumnType("date");

                entity.HasOne(d => d.AccType)
                    .WithMany(p => p.AccBalances)
                    .HasForeignKey(d => d.AccTypeId)
                    .HasConstraintName("FK_AccBalance_AccType");
            });

            modelBuilder.Entity<AccType>(entity =>
            {
                entity.ToTable("AccType");

                entity.Property(e => e.AccTypeId).ValueGeneratedNever();

                entity.Property(e => e.AccName).HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
